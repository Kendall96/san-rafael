<?php

  if(!isset($_SESSION['nombre'])){//comprueba si existe el nombre de usuario
    redirect("principal/index"); 
  }

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Clientes</title>
    <!--<link href="<?php //echo base_url(); ?>/assets/css/bootstrap.css" rel="stylesheet">-->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>/assets/img/supersanrafael.ico">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/styles.css">
  </head>

  <body>
    <div class="container">

      <div>
        <a href="<?= base_url('Usuarios/user_view') ?>">
          <img id="logoSuper" src="<?php echo base_url(); ?>/assets/img/supersanrafael.jpeg" alt="Logo principal" />
        </a>
      </div>

      <br /> <br /> <br />
      <br /> <br /> <br />

      <div class="panel panel-info" style="margin-top: 1%;">
        
        <!--<div class="panel-heading" style="display: flex; text-align: center;">
            <h3 class="panel-title" style="margin-top: 5px;">Clientes</h3>
            <a id="btnAdd" class="new btn btn-sm btn-primary" href="<php= base_url('Clientes/registro') ?>">Agregar</a>
        </div>-->

        <div class="panel-heading" style="display: flex; text-align: center; background-color: #ec3128;">
          <h3 class="panel-title" style="margin-top: 5px; font-size: 150%; color: white; margin-left: 2%;">Clientes</h3>
          <a style="margin-top: 0.20%; margin-left: 81.5%; height: 1%; color:white;" class="btn btn-outline-dark" href="<?= base_url('Clientes/registro') ?>">Agregar</a>
        </div>

        <div class="panel-body detalle-producto">

          <?php if($clientes != false){?>
            <table class="table">

              <thead>
                <tr>
				  				<th>Cedula</th>
                  <th>Nombre</th>
                  <th>Apellidos</th>
                  <th>Telefono</th>
                  <th>Direccion</th>
                  <th>Cedula Fiador</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>

              <tbody>
                  <?php
                // $total = 0;
                    foreach($clientes as $item){
                  ?>
                  <tr>
                    <?php 
                      $cedula = $item['cedula'];           
                    ?>

										<td><?php  
                    echo $item['cedula'];
                    ?></td>

                    <td><?php 
                    echo $item['nombre'];
                    ?></td>

                    <td><?php 
                      echo $item['apellidos'];
                    ?></td>

                    <td><?php 	
											echo $item['telefono']; 
										?></td>

                    <td><?php 
                      echo $item['direccion'];
                    ?></td>

										<td>
                      <?php 
                        if($item['id_fiador'] == 0){ echo 'Sin Fiador'; }else{ echo $item['id_fiador']; };
                      ?>
                    </td>

										<td><a class="btn btn-sm btn-primary" href="<?php echo base_url() . "Clientes/Cliente/" . $cedula?>">Editar</a></td>
										<!--<td><a class="btn btn-sm btn-danger" href="<?php //echo base_url() . "Clientes/desactivar/" . $cedula?>">Eliminar</a></td>-->

                  </tr>
                          
                  <?php }?>
                    
                  </tr>
              </tbody>
              
            </table>

            <?php }else{?>
              
            <div class="container" style="margin-left: 40%; margin-top: 10%;"> No hay clientes agregados</div>

          <?php }?>
        </div>
		  </div>
    </div>
  </body>
</html>
