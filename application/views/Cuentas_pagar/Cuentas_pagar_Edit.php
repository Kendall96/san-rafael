<?php

  if(!isset($_SESSION['nombre'])){//comprueba si existe el nombre de usuario
    redirect("principal/index"); 
  }

?>

<!DOCTYPE html> 
<html lang="en">
  <head>
    <title>Cuentas Proveedor</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>/assets/img/supersanrafael.ico">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen" title="no title">-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/styles.css">
  </head>

  <body>
    <div class="container">

      <div>
        <a href="<?= base_url('Cuentas_pagar/getCuentas_pagar') ?>">
          <img id="logoSuper" src="<?php echo base_url(); ?>/assets/img/supersanrafael.jpeg" alt="Logo principal" />
        </a>
      </div>

      <br /> <br /> <br />
      <br /> <br /> <br />
      
      <div class="panel panel-info" style="margin-top: 1%;">

        <div class="panel-heading" style="display: flex; text-align: center; background-color: #ec3128;">
          <h3 class="panel-title" style="margin-top: 8px; font-size: 150%; color: white; margin-left: 2%;">Editar Cuenta</h3>
        </div>

        <div class="panel-body detalle-producto">
            <?php foreach($cuenta as $item){?>
                <div class="container" style="margin-left: 2.5%;">
                    <form method="post" action="<?php echo base_url() . "Cuentas_pagar/editar/" . $item['id']?>">
                                      
                      <div class="form-group" id="datos2">
                        <label for="numero_factura">Numero de factura</label>
                        <input class="form-control" placeholder="Numero de factura" name="numero_factura" type="text" onkeypress="return validar(event)" required
                            value="<?php  echo $item['numero_factura']; ?>">
                      </div>

                      <div class="form-group">
                        <label for="proveedor">Proveedor</label>
                        <div class="form-group col-md-13">
                          <select style="max-width:95%;" id="proveedor" name="id_proveedor" value="" class="form-control" required>
                            <option value="">Seleccionar proveedor</option>

                            <?php if(count($provedor)>0):?>
                              <?php $valor = $item['id_proveedor'];?>
                              <?php foreach($provedor as $pro):?>
                                <option value="<?php echo $pro['id'];?>" <?php if($pro['id'] == $valor){ echo "selected"; }?>><?php echo $pro['nombre_pv'];?></option>
                              <?php endforeach;?>
                            <?php endif;?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group" id="datos2">
                        <label for="monto">Monto</label>
                        <input class="form-control" placeholder="Monto" name="monto" type="text" onkeypress="return validar(event)" required
                            value="<?php echo $item['monto'];?>">
                      </div>

                      <div class="form-group">
                        <label for="pago">Selecionar fecha</label>
                        <input style="max-width:95%;" class="form-control" placeholder="Fecha de pago" name="fecha_pago" type="date" onkeypress="return validar(event)" required
                            value="<?php echo $item['fecha_pago'];?>">
                      </div>

                      <input style="margin-top: -35.9%; margin-left: 91.5%; height: 1%; color:white;" id="botoen" class="btn btn-outline-dark" type="submit" value="Editar" name="Editar">
                    </form>
                </div>
          <?php }?>
        </div>
        <script>

              function soloLetras(e) {//esta funcion valida si la tecla presionada esta permitida
                  key = e.keyCode || e.which;
                  tecla = String.fromCharCode(key).toLowerCase();
                  letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
                  especiales = [8, 37, 39, 46];

                  tecla_especial = false
                  for(var i in especiales) {
                      if(key == especiales[i]) {
                          tecla_especial = true;
                          break;
                      }
                  }

                  if(letras.indexOf(tecla) == -1 && !tecla_especial){
                      return false;}
              }

              function limpia() {
                  var val = document.getElementById("miInput").value;
                  var tam = val.length;
                  for(i = 0; i < tam; i++) {
                      if(!isNaN(val[i]))
                          document.getElementById("miInput").value = '';
                  }
              }

              function validar(e) { // valida solo que solo se pueda ingresar numeros
                  tecla = (document.all) ? e.keyCode : e.which;
                  if (tecla==8) return true;
                  patron =/\d/;
                  te = String.fromCharCode(tecla);
                  return patron.test(te);
              } 

          </script>
      </div>
    </div>

    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
