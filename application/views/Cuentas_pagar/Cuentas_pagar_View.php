<?php

  if(!isset($_SESSION['nombre'])){//comprueba si existe el nombre de usuario
    redirect("principal/index"); 
  }

?>

<!DOCTYPE html>
<html lang="en"> 
  <head>
    <title>Cuentas Proveedores</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>/assets/img/supersanrafael.ico">
	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/styles.css">
  </head>

  <body>
    <div class="container">
      <div>
        <a href="<?= base_url('Usuarios/user_view') ?>">
          <img id="logoSuper" src="<?php echo base_url(); ?>/assets/img/supersanrafael.jpeg" alt="Logo principal" />
        </a>
      </div>

      <br /> <br /> <br />
      <br /> <br /> <br />
      
      <div class="panel panel-info" style="margin-top: 1%;">

        <div class="panel-heading" style="display: flex; text-align: center; background-color: #ec3128;">
          <h3 class="panel-title" style="margin-top: 5px; font-size: 150%; white: 15%; color: white; margin-left: 2%;">Pagos_Proveedor</h3>
          <a style="margin-top: 0.20%; margin-left: 72%; height: 1%; color:white;" class="btn btn-outline-dark" href="<?= base_url('Cuentas_pagar/registro') ?>">Agregar</a>
        </div>

        <div class="panel-body detalle-producto">

          <?php if($Cuentas_pagar != false){?>
            <table class="table">

              <thead>
                <tr>
				  				<th>Numero de factura</th>
                  <th>Proveedor</th>
                  <th>Monto</th>
                  <th>Fecha Pago</th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>

              <tbody>
                <?php
                	
                foreach($Cuentas_pagar as $item){?>
                    
                  <tr>
                    <?php  
                      $id = $item['id'];
                    ?>

                    <td><?php  
                      echo $item['numero_factura'];
                    ?></td>

                    <td><?php  
                      echo $item['nombre_pv'];
                    ?></td>

                    <td>₡ <?php 
                      echo $item['monto'];
                    ?></td>

                    <td><?php 
                        echo $item['fecha_pago'];
                    ?></td>
                    <td><a class="btn btn-sm btn-primary" href="<?php echo base_url() . "Cuentas_pagar/cuenta/" . $id?>">Editar</a></td>
                    <td><a class="btn btn-sm btn-danger" href="<?php echo base_url() . "Cuentas_pagar/desactivar/" . $id?>">Eliminar</a></td>

                  </tr>
                
                <?php }?>
              </tbody>
            </table>
										
            <?php }else{?>

            <div class="container" style="margin-left: 40%; margin-top: 10%;"> No hay pagos a realizar agregados</div>

          <?php }?>
        </div>
		  </div>
    </div>
  </body>
</html>
