<?php

  if(!isset($_SESSION['nombre'])){//comprueba si existe el nombre de usuario
    redirect("principal/index"); 
  }

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Proveedores</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>/assets/img/supersanrafael.ico">
    <!--<link href="<?php //echo base_url(); ?>/assets/css/bootstrap.css" rel="stylesheet">-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/styles.css">
  </head>

  <body>
    <div class="container">

      <div>
        <a href="<?= base_url('Usuarios/user_view') ?>">
          <img id="logoSuper" src="<?php echo base_url(); ?>/assets/img/supersanrafael.jpeg" alt="Logo principal" />
        </a>
      </div>

      <br /> <br /> <br />
      <br /> <br /> <br />

      <div class="panel panel-info" style="margin-top: 1%;">
        
        <!--<div class="panel-heading" style="display: flex; text-align: center;">
            <h3 class="panel-title" style="margin-top: 5px;">Proveedores</h3>
            <a style="margin-left: 965px;" class="new btn btn-sm btn-primary" href="<?php //base_url('Proveedores/registro') ?>">Agregar</a>
        </div>-->

        <div class="panel-heading" style="display: flex; text-align: center; background-color: #ec3128;">
            <h3 class="panel-title" style="margin-top: 5px; font-size: 150%; color: white; margin-left: 2%;">Proveedores</h3>
            <a style="margin-top: 0.20%; margin-left: 77.5%; height: 1%; color:white;" class="btn btn-outline-dark" href="<?= base_url('Proveedores/registro') ?>">Agregar</a>
        </div>

        <div class="panel-body detalle-producto">

          <?php if($proveedores != false){?>
            <table class="table">

              <thead>
                <tr>
				  				<th>ID</th>
                  <th>Nombre</th>
                  <th>Direccion</th>
                  <th>Telefono</th>
                  <th>Correo</th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>

            	<tbody>

                  <?php foreach($proveedores as $item){?>
              		<tr>
											<?php 
											$id = $item['id'];           
											?>

											<td><?php  
												echo $item['id'];
											?></td>

											<td><?php 
											echo $item['nombre_pv'];
											?></td>

											<td><?php 
												echo $item['direccion'];
											?></td>

											<td><?php 	
												echo $item['telefono']; 
											?></td>

											<td><?php 
												echo $item['correo'];
											?></td>

											<td><a class="btn btn-sm btn-primary" href="<?php echo base_url() . "Proveedores/Proveedor/" . $id?>">Editar</a></td>
											<td><a class="btn btn-sm btn-danger" href="<?php echo base_url() . "Proveedores/desactivar/" . $id?>">Eliminar</a></td>
  
                  </tr>
                          
                  <?php }?>
                    
                </tr>
              </tbody>
              
            </table>

            <?php }else{?>

            <div class="panel-body"> No hay proveedores agregados</div>

          <?php }?>
        </div>
		  </div>
    </div>
  </body>
</html>
