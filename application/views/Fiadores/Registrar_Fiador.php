<?php

  if(!isset($_SESSION['nombre'])){//comprueba si existe el nombre de usuario
    redirect("principal/index"); 
  }

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Fiadores</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>/assets/img/supersanrafael.ico">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen" title="no title">-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/styles.css">
  </head>

  <body>
    <div class="container">

      <div>
        <a href="<?= base_url('Fiadores/getFiadores') ?>">
          <img id="logoSuper" src="<?php echo base_url(); ?>/assets/img/supersanrafael.jpeg" alt="Logo principal" />
        </a>
      </div>

      <br /> <br /> <br />
      <br /> <br /> <br />
      
      <div class="panel panel-info" style="margin-top: -1%;"> <!--background-color: #1e3a8e;-->

        <div class="panel-heading" style="display: flex; text-align: center; background-color: #ec3128;">
          <h3 class="panel-title" style="margin-top: 8px; font-size: 150%; color: white; margin-left: 2%;">Registrar Fiadores</h3>
        </div>

        <div class="panel-body detalle-producto">
          <div class="container" style="margin-left: 2.5%;">

            <?php if(!empty($this->session->flashdata())): ?>
                <div class="alert alert-<?php echo $this->session->flashdata('clase')?>" id="mensa12">
                    <?php $error = true; 
                    echo $this->session->flashdata('mensaje') ?>

                </div>
            <?php endif; ?>

            <form method="post" action="<?php echo base_url('Fiadores/nuevo_fiador');?>" style="position: relative;">
              <div class="form-group" id="datos2">
                <label for="cedula">Cedula</label>
                <input class="form-control" placeholder="Cedula" name="cedula" type="text"  onkeypress="return validar(event)"  required
                    value="<?php
                    if(isset($error)){
                        echo $this->session->flashdata('cedula');
                    }    
                ?>">
              </div>
              
              <div class="form-group">
                <label for="nombre">Nombre</label>
                <input class="form-control" placeholder="Nombre" name="nombre" type="text" autofocus onkeypress="return soloLetras(event)" onblur="limpia()" id="miInput" required
                    value="<?php
                    if(isset($error)){
                        echo $this->session->flashdata('nombre');
                    }    
                ?>">
              </div>

              <div class="form-group">
                <label for="apellidos">Apellidos</label>
                <input class="form-control" placeholder="Apellidos" name="apellidos" type="text" autofocus onkeypress="return soloLetras(event)" onblur="limpia()" id="miInput" required
                    value="<?php
                    if(isset($error)){
                        echo $this->session->flashdata('apellidos');
                    }    
                ?>">
              </div>

              <div class="form-group">
                <label for="telefono">Telefono</label>
                <input class="form-control" placeholder="Telefono" name="telefono" type="text" onkeypress="return validar(event)" required
                    value="<?php
                    if(isset($error)){
                        echo $this->session->flashdata('telefono');
                    }    
                ?>">
              </div>

              <div class="form-group">
                <label for="direccion">Direccion</label>
                <input class="form-control" placeholder="Direccion" name="direccion" type="text" id="miInput" required
                    value="<?php
                    if(isset($error)){
                        echo $this->session->flashdata('direccion');
                    }    
                ?>">
              </div>

              <input style="position: relative; margin-top: -42.9%; margin-left: 88%; height: 1%; color:white;" id="botoen" class="btn btn-outline-dark" class="btn btn-lg  btn-block" type="submit" value="Registrarse" name="Registrarse"> 
            </form>
          </div>
        </div>
        <script>

          function soloLetras(e) {//esta funcion valida si la tecla presionada esta permitida
              key = e.keyCode || e.which;
              tecla = String.fromCharCode(key).toLowerCase();
              letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
              especiales = [8, 37, 39, 46];

              tecla_especial = false
              for(var i in especiales) {
                  if(key == especiales[i]) {
                      tecla_especial = true;
                      break;
                  }
              }

              if(letras.indexOf(tecla) == -1 && !tecla_especial){
                  return false;}
          }

          function limpia() {
              var val = document.getElementById("miInput").value;
              var tam = val.length;
              for(i = 0; i < tam; i++) {
                  if(!isNaN(val[i]))
                      document.getElementById("miInput").value = '';
              }
          }

          function validar(e) { // valida solo que solo se pueda ingresar numeros
              tecla = (document.all) ? e.keyCode : e.which;
              if (tecla==8) return true;
              patron =/\d/;
              te = String.fromCharCode(tecla);
              return patron.test(te);
          } 

        </script>
      </div>
    </div>

    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>