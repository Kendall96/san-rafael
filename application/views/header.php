<?php

?>

<!DOCTYPE html>
<html lang="en"> 

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Minisuper San Rafel</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>/assets/img/supersanrafael.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen" title="no title">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/index.css">
    <script src = "https://code.jquery.com/jquery-3.0.0.js"> </script>

</head>

<body>
    <div id="container">

        <!--header-->
        <header id="header">
            <div>
                <img id="logoSuper1" src="<?php echo base_url(); ?>/assets/img/supersanrafael.jpeg" alt="Logo principal" />
            </div>
        </header>
